particlesJS.load('particles-js', 'particles.json');

$(document).ready(function(){
    // description on main page
    var text = ["Pythonist", "Deep Learning Enthusiast", "Volunteer", "Free Software Supporter","i3wm","Emacs","GNU/Linux User"];
    var index = 0;
    var description = $("#skillz");

    description.fadeOut("slow", changeText);

    function changeText(){
        description.html(text[index]).fadeIn("slow", function() {

            description.delay(1000).fadeOut("slow", function() {
                index++;
                if (index == text.length) {
                    index = 0;
                };
                setTimeout(changeText, 400);
            });
        });
    }
});
